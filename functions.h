#ifndef FUNCTIONS_H
#define FUNCTIONS_H


int vc_rgb_get_red_gray1Channel(IVC *src,IVC *dst);
int vc_rgb_get_red_gray(IVC *srcdst);
int vc_rgb_get_green_gray(IVC *srcdst);
int vc_rgb_negative(IVC *srcdst);
int vc_rgb_to_gray(IVC *src, IVC *dst);
int vc_rgb_get_red(IVC *srcdst);
int vc_rgb_to_hsv(IVC *srcdst);
int vc_histogram_equalize(IVC* srcdst);



int vc_gray_negative(IVC *srcdst);
int vc_gray_to_binary_bernsen(IVC *src, IVC *dst);
int vc_gray_to_binary_global_mean(IVC *srcdst);
int vc_gray_to_binary_midpoint(IVC *src, IVC *dst, int NxM);


int vc_binary_blob_labelling_v0(IVC *srcdst);
int vc_binary_blob_labelling_v2(IVC *srcdst);
int vc_binary_dilate(IVC *src, IVC *dst, int size);



#endif /* 	FUNCTIONS_H */







