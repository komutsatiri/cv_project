#include <stdio.h>
#include "vc.h"
#include "functions.h"


int main(void)
{
	IVC *image;
	


	image = vc_read_image("./images/do_01.ppm");

	if(image==NULL)
	{
		printf("ERROR -> vc_read_image():\n\tfile not found\n");
		getchar();
		return 0;
	}

	vc_rgb_get_green_gray(image);
	vc_histogram_equalize(image);
	vc_write_image("./images/do_01_histo_eq.ppm",image);


	vc_image_free(image);
	;

	printf("Press any key to exit.. \n");
	return 1;
}