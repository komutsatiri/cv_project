#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "vc.h"

int vc_gray_to_binary_bernsen(IVC *src, IVC *dst)
{
	unsigned char *datasrc = (unsigned char *) src->data;
	unsigned char *datadst = (unsigned char *) dst->data;
	int width = src->width;
	int height = src->height;
	int bytesperline = src->bytesperline;
	int channels = src->channels;
	int x, y;
	int xx, yy;
	int max, min;
	long int pos, posk;
	unsigned char threshold;

	// Verificação de erros
	if((src->width <= 0) || (src->height <= 0) || (src->data == NULL)) return 0;
	if((src->width != dst->width) || (src->height != dst->height) || (src->channels != dst->channels)) return 0;
	if(channels != 1) return 0;

	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;

			max = datasrc[pos];
			min = datasrc[pos];

			// 25 Vizinhos
			for(yy=-12; yy<=12; yy++)
			{
				for(xx=-12; xx<=12; xx++)
				{
					if((y + yy > 0) && (y + yy < height) && (x + xx > 0) && (x + xx < width))
					{
						posk = (y + yy) * bytesperline + (x + xx) * channels;

						if(datasrc[posk] > max) max = datasrc[posk];
						if(datasrc[posk] < min) min = datasrc[posk];
					}
				}
			}

			if(max - min < 15 /*Cmin*/) threshold = src->levels / 2;
			else threshold = (unsigned char) ((float) (max + min) / (float) 2);

			if(datasrc[pos] > threshold) datadst[pos] = 255;
			else datadst[pos] = 0;
		}
	}

	return 1;
}

int vc_gray_to_binary_global_mean(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;
	int x, y;
	long int pos;
	long int sum = 0;
	int threshold;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 1) return 0;

	// Cálculo do threshold através da média global
	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;

			sum += data[pos];
		}
	}
	threshold = sum / (width * height);

#ifdef VC_DEBUG
	printf("MESSAGE -> vc_gray_to_binary_global_mean():\n\tthreshold = %d\n", threshold);
#endif

	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;

			if(data[pos] <= (unsigned char) threshold) data[pos] = 0;
			else data[pos] = 255;
		}
	}

	return 1;
}

int vc_gray_to_binary_midpoint(IVC *src, IVC *dst, int NxM)
{
	unsigned char *datasrc = (unsigned char *) src->data;
	unsigned char *datadst = (unsigned char *) dst->data;
	int width = src->width;
	int height = src->height;
	int bytesperline = src->bytesperline;
	int channels = src->channels;
	int x, y;
	int xx, yy;
	int xxyymax = (int) floor(sqrt((double) NxM) / 2);
	int xxyymin = -xxyymax;
	int max, min;
	long int pos, posk;
	unsigned char threshold;

	// Verificação de erros
	if((src->width <= 0) || (src->height <= 0) || (src->data == NULL)) return 0;
	if((src->width != dst->width) || (src->height != dst->height) || (src->channels != dst->channels)) return 0;
	if(channels != 1) return 0;

	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;

			max = datasrc[pos];
			min = datasrc[pos];

			// NxM Vizinhos
			for(yy=xxyymin; yy<=xxyymax; yy++)
			{
				for(xx=xxyymin; xx<=xxyymax; xx++)
				{
					if((y + yy >= 0) && (y + yy < height) && (x + xx >= 0) && (x + xx < width))
					{
						posk = (y + yy) * bytesperline + (x + xx) * channels;

						if(datasrc[posk] > max) max = datasrc[posk];
						if(datasrc[posk] < min) min = datasrc[posk];
					}
				}
			}

			threshold = (unsigned char) ((float) (max + min) / (float) 2);

			if(datasrc[pos] > threshold) datadst[pos] = 255;
			else datadst[pos] = 0;
		}
	}

	return 1;
}

// Gerar negativo da imagem Gray
int vc_gray_negative(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;
	int x, y;
	long int pos;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 1) return 0;

	// Inverte a imagem Gray
	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;
			data[pos] = 255 - data[pos];
		}
	}
	return 1;
}

int vc_rgb_get_red_gray(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;
	int x, y;
	long int pos;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 3) return 0;

	// Extrai a componente Red
	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;

			data[pos + 1] = data[pos]; // Green
			data[pos + 2] = data[pos]; // Blue
		}
	}

	return 1;
}

int vc_rgb_get_red_gray1Channel(IVC *src,IVC *dst)
{
	unsigned char *datasrc = (unsigned char *) src->data;
	unsigned char *datadst = (unsigned char *) dst->data;
	int width = src->width;
	int height = src->height;
	int bytesperline = src->bytesperline;
	int channels = src->channels;
	int x, y;
	long int pos;

	// Verificação de erros
	if((src->width <= 0) || (src->height <= 0) || (src->data == NULL)) return 0;
	if((src->width != dst->width) || (src->height != dst->height) || (src->channels != 3) || (dst->channels != 1)) return 0;

	// Extrai a componente Red
	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;
			datadst[pos/3] = datasrc[pos];
		}
	}

	return 1;
}

int vc_rgb_negative(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;
	int x, y;
	long int pos;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 3) return 0;

	// Inverte a imagem RGB
	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;

			data[pos] = 255 - data[pos];
			data[pos + 1] = 255 - data[pos + 1];
			data[pos + 2] = 255 - data[pos + 2];
		}
	}

	return 1;
}

// Etiquetagem de blobs
int vc_binary_blob_labelling_v0(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;

	int i;
	int x, y, a, b;
	long int posX, posA, posB, posC, posD;

	int label = 1; // Etiqueta inicial
	int num;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 1) return 0;

	// Altera o nível para 255 - Recebemos uma imagem binário e o output é uma imagem com etiquetas!
	srcdst->levels = 255;

	// Limpa os rebordos da imagem binária
	for(y=0; y<height; y++)
	{
		data[y * bytesperline + 0 * channels] = 0;
		data[y * bytesperline + (width-1) * channels] = 0;
	}
	for(x=0; x<width; x++)
	{
		data[0 * bytesperline + x * channels] = 0;
		data[(height-1) * bytesperline + x * channels] = 0;
	}

	// Efectua a etiquetagem
	for(y=1; y<height-1; y++)
	{
		for(x=1; x<width-1; x++)
		{
			// Kernel:
			// A B C
			// D X 

			posA = (y-1) * bytesperline + (x-1) * channels; // A
			posB = (y-1) * bytesperline + x * channels; // B
			posC = (y-1) * bytesperline + (x+1) * channels; // C
			posD = y * bytesperline + (x-1) * channels; // D

			posX = y * bytesperline + x * channels; // X

			// Se o pixel foi marcado
			if(data[posX] != 0)
			{
				if((data[posA] == 0) && (data[posB] == 0) && (data[posC] == 0) && (data[posD] == 0))
				{
					data[posX] = label;
					label++;

				}
				else
				{
					num = 255;

					// Se A está marcado, já tem etiqueta (já não é 255), e é menor que a etiqueta "num"
					if((data[posA] != 0) && (data[posA] != 255) && (data[posA] < num))
					{
						num = data[posA];
					}
					// Se B está marcado, já tem etiqueta (já não é 255), e é menor que a etiqueta "num"
					if((data[posB] != 0) && (data[posB] != 255) && (data[posB] < num))
					{
						num = data[posB];
					}
					// Se C está marcado, já tem etiqueta (já não é 255), e é menor que a etiqueta "num"
					if((data[posC] != 0) && (data[posC] != 255) && (data[posC] < num))
					{
						num = data[posC];
					}
					// Se D está marcado, já tem etiqueta (já não é 255), e é menor que a etiqueta "num"
					if((data[posD] != 0) && (data[posD] != 255) && (data[posD] < num))
					{
						num = data[posD];
					}

					data[posX] = num;
				}
			}
		}
	}

	printf("\nNumero de objectos = %d\n", num);

	return 1;
}

int vc_binary_dilate(IVC *src, IVC *dst, int size)
{
	unsigned char *datasrc = (unsigned char *) src->data;
	unsigned char *datadst = (unsigned char *) dst->data;
	int width = src->width;
	int height = src->height;
	int bytesperline = src->bytesperline;
	int channels = src->channels;
	int x, y;
	int xk, yk;
	int i, j;
	long int pos, posk;
	int s1, s2;
	int x1, y1; // Coordenada do primeiro pixel da imagem a ser processado
	int x2, y2; // Coordenada do último pixel da imagem a ser processado
	unsigned char pixel;

	// Verificação de erros
	if((src->width <= 0) || (src->height <= 0) || (src->data == NULL)) return 0;
	if((src->width != dst->width) || (src->height != dst->height) || (src->channels != dst->channels)) return 0;
	if(channels != 1) return 0;

	s2 = size / 2;
	//s1 = -(size - s2);
	s1 = -(s2);

	x1 = -s1;
	y1 = -s1;
	x2 = width - s2;
	y2 = height - s2;

	memcpy(datadst, datasrc, bytesperline * height);

	// Remove possíveis pixéis de primeiro plano, nos limites da imagem
	for(y=0; y<y1; y++) for(x=0; x<width; x++) datadst[y * bytesperline + x * channels] = 0;
	for(y=y2; y<height; y++) for(x=0; x<width; x++) datadst[y * bytesperline + x * channels] = 0;
	for(x=0; x<x1; x++) for(y=0; y<height; y++) datadst[y * bytesperline + x * channels] = 0;
	for(x=x2; x<width; x++) for(y=0; y<height; y++) datadst[y * bytesperline + x * channels] = 0;

	// Cálculo da dilatação
	for(y=y1; y<y2; y++)
	{
		for(x=x1; x<x2; x++)
		{
			pos = y * bytesperline + x * channels;

			pixel = datasrc[pos];

			for(yk=s1; yk<=s2; yk++)
			{
				j = y + yk;

				for(xk=s1; xk<=s2; xk++)
				{
					i = x + xk;

					posk = j * bytesperline + i * channels;

					pixel |= datasrc[posk];
				}
			}

			// Se um qualquer pixel da vizinhança, na imagem de origem, for de primeiro plano, então o pixel central
			// na imagem de destino é também definido como de primeiro plano.
			if(pixel != 0) datadst[pos] = 255;
		}
	}

	return 1;
}

// Conversão de RGB para HSV
int vc_rgb_to_hsv(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;
	float r, g, b, hue, saturation, value;
	float rgb_max, rgb_min;
	int i, size;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 3) return 0;
	
	size = width * height * channels;
	
	for(i=0; i<size; i=i+channels)
	{
		r = (float) data[i];
		g = (float) data[i + 1];
		b = (float) data[i + 2];
		
		// Calcula valores máximo e mínimo dos canais de cor R, G e B
		rgb_max = (r > g ? (r > b ? r : b) : (g > b ? g : b));
		rgb_min = (r < g ? (r < b ? r : b) : (g < b ? g : b));
		
		// Value toma valores entre [0,255]
		value = rgb_max;
		if(value == 0.0)
		{
			hue = 0.0;
			saturation = 0.0;
		}
		else
		{
			// Saturation toma valores entre [0,255]
			saturation = ((rgb_max - rgb_min) / rgb_max) * (float) 255.0;

			if(saturation == 0.0)
			{
				hue = 0.0;
			}
			else
			{
				// Hue toma valores entre [0,360]
				if((rgb_max == r) && (g >= b))
				{
					hue = 60.0f * (g - b) / (rgb_max - rgb_min);
				}
				else if((rgb_max == r) && (b > g))
				{
					hue = 360.0f + 60.0f * (g - b) / (rgb_max - rgb_min);
				}
				else if(rgb_max == g)
				{
					hue = 120 + 60 * (b - r) / (rgb_max - rgb_min);
				}
				else /* rgb_max == b*/
				{
					hue = 240.0f + 60.0f * (r - g) / (rgb_max - rgb_min);
				}
			}
		}

		// Atribui valores entre [0,255]
		data[i] = (unsigned char) (hue / 360.0 * 255.0);
		data[i + 1] = (unsigned char) (saturation);
		data[i + 2] = (unsigned char) (value);
		
		/*data[i] = 0;
		data[i + 1] = 0;
		data[i + 2] = 0;*/

		/*if(value < 0.1) { data[i] = 0; data[i + 1] = 0; data[i + 2] = 0;} // Preto
		else if((value > 0.9) && (saturation < 0.1)) { data[i] = 255; data[i + 1] = 255; data[i + 2] = 255;} // Branco
		else if(saturation < 0.1) { data[i] = 127; data[i + 1] = 127; data[i + 2] = 127;} // Cinzento
		else if((hue > 330) || (hue <= 30)) { data[i] = data[i]; data[i + 1] = 0; data[i + 2] = 0;} // Vermelho
		else if((hue > 30) && (hue <= 90)) { data[i] = data[i]; data[i + 1] = data[i + 1]; data[i + 2] = 0;} // Amarelo
		else if((hue > 90) && (hue <= 150)) { data[i] = 0; data[i + 1] = data[i + 1]; data[i + 2] = 0;} // Verde
		else if((hue > 150) && (hue <= 210)) { data[i] = 0; data[i + 1] = data[i + 1]; data[i + 2] = data[i + 2];} // Ciano
		else if((hue > 210) && (hue <= 270)) { data[i] = 0; data[i + 1] = 0; data[i + 2] = data[i + 2];} // Azul
		else { data[i] = data[i]; data[i + 1] = 0; data[i + 2] = data[i + 2];} // Magenta*/

		/*if((hue > 30) && (hue <= 60) && (saturation >= 60) && (value >= 60))  // Amarelo
		{
			data[i] = 255; data[i + 1] = 255; data[i + 2] = 0;
		}
		if((hue > 65) && (hue <= 150) && (saturation >= 40) && (value >= 12)) // Verde
		{
			data[i] = 0; data[i + 1] = 255; data[i + 2] = 0;
		}*/

		//if(!((hue > 30) && (hue <= 90) && (saturation >= 60) && (value >= 60)))  // Amarelo
		//{
		//	data[i] = 255; data[i + 1] = 255; data[i + 2] = 255;
		//}
		//if(!((hue > 90) && (hue <= 150) && (saturation >= 60) && (value >= 60)))  // Verde
		//{
		//	data[i] = 255; data[i + 1] = 255; data[i + 2] = 255;
		//}
		//if(!((hue > 210) && (hue <= 270) && (saturation >= 60) && (value >= 60)))  // Azul
		//{
		//	data[i] = 255; data[i + 1] = 255; data[i + 2] = 255;
		//}
	}

	return 1;
}

int vc_rgb_to_gray(IVC *src, IVC *dst)
{


	unsigned char *datasrc = (unsigned char *) src->data;
	int bytesperline_src = src->width * src->channels;
	int channels_src = src->channels;
	unsigned char *datadst = (unsigned char *) dst->data;
	int bytesperline_dst = dst->width * dst->channels;
	int channels_dst = dst->channels;
	int width = src->width;
	int height = src->height;
	int x,y;
	long int pos_src,pos_dst;
	float rf,gf,bf;


	// Error verification
	if((src->width <= 0) || (src->height <= 0) || (src->data == NULL)) 
		
		return 0;
	if((src->width != dst->width) || (src->height =! dst->height ))
		return 0;
	if((src->channels != 3) || (dst->channels != 1))
		return 0;		

	for (y = 0; y < height; y++) {
		for (x = 0; x < width; x++) {
			pos_src = y * bytesperline_src + x * channels_src;
			pos_dst = y * bytesperline_dst + x * channels_dst;

			rf = (float) datasrc[pos_src]; 
			gf=  (float) datasrc[pos_src+1];
			bf = (float) datasrc[pos_src+2];
			


			datadst[pos_dst] = (unsigned char) ((rf*0.299)+(gf*0.587)+(bf*0.114));

		}
	}

	return 1;
}


int vc_rgb_get_red(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;
	int x, y;
	long int pos;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 3) return 0;

	// Extrai a componente Red
	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;

			data[pos + 1] = 0; // Green
			data[pos + 2] = 0; // Blue
		}
	}

	return 1;
}




int vc_binary_blob_labelling_v2(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;

	int i;
	int x, y, a, b;
	long int posX, posA, posB, posC, posD;

	int labeltable[256] = {0};
	int label = 1; // Etiqueta inicial
	int num;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 1) return 0;

	// Altera o nível para 255 - Recebemos uma imagem binário e o output é uma imagem com etiquetas!
	srcdst->levels = 255;

	// Limpa os rebordos da imagem binária
	for(y=0; y<height; y++)
	{
		data[y * bytesperline + 0 * channels] = 0;
		data[y * bytesperline + (width-1) * channels] = 0;
	}
	for(x=0; x<width; x++)
	{
		data[0 * bytesperline + x * channels] = 0;
		data[(height-1) * bytesperline + x * channels] = 0;
	}

	// Efectua a etiquetagem
	for(y=1; y<height-1; y++)
	{
		for(x=1; x<width-1; x++)
		{
			// Kernel:
			// A B C
			// D X 

			posA = (y-1) * bytesperline + (x-1) * channels; // A
			posB = (y-1) * bytesperline + x * channels; // B
			posC = (y-1) * bytesperline + (x+1) * channels; // C
			posD = y * bytesperline + (x-1) * channels; // D

			posX = y * bytesperline + x * channels; // X

			// Se o pixel foi marcado
			if(data[posX] != 0)
			{
				if((data[posA] == 0) && (data[posB] == 0) && (data[posC] == 0) && (data[posD] == 0))
				{
					data[posX] = label;
					labeltable[label] = label;
					label++;

					//for(i=0; i<label; i++)
					//	printf("%d %d\n", i, labeltable[i]);
					//printf("end	\n");

				}
				else
				{
					num = 255;

					// Se A está marcado, já tem etiqueta (já não é 255), e é menor que a etiqueta "num"
					if((data[posA] != 0) && (data[posA] != 255) && (data[posA] < num))
					{
						num = data[posA];
					}
					// Se B está marcado, já tem etiqueta (já não é 255), e é menor que a etiqueta "num"
					if((data[posB] != 0) && (data[posB] != 255) && (data[posB] < num))
					{
						num = data[posB];
					}
					// Se C está marcado, já tem etiqueta (já não é 255), e é menor que a etiqueta "num"
					if((data[posC] != 0) && (data[posC] != 255) && (data[posC] < num))
					{
						num = data[posC];
					}
					// Se D está marcado, já tem etiqueta (já não é 255), e é menor que a etiqueta "num"
					if((data[posD] != 0) && (data[posD] != 255) && (data[posD] < num))
					{
						num = data[posD];
					}

					// Actualiza a tabela de etiquetas
					if((data[posA] != 0) && (data[posA] != 255))
					{
						if(labeltable[data[posA]] != labeltable[num])
						{
							for(a=1; a<label; a++)
							{
								//todos os objectos marcados 
								if(labeltable[a] == labeltable[data[posA]])
								{
									labeltable[a] = labeltable[num];
								}
							}
						}
					}
					if((data[posB] != 0) && (data[posB] != 255))
					{
						if(labeltable[data[posB]] != labeltable[num])
						{
							for(a=1; a<label; a++)
							{
								if(labeltable[a] == labeltable[data[posB]])
								{
									labeltable[a] = labeltable[num];
								}
							}
						}
					}
					if((data[posC] != 0) && (data[posC] != 255))
					{
						if(labeltable[data[posC]] != labeltable[num])
						{
							for(a=1; a<label; a++)
							{
								if(labeltable[a] == labeltable[data[posC]])
								{
									labeltable[a] = labeltable[num];
								}
							}
						}
					}
					if((data[posD] != 0) && (data[posD] != 255))
					{
						if(labeltable[data[posD]] != labeltable[num])
						{
							for(a=1; a<label; a++)
							{
								if(labeltable[a] == labeltable[data[posD]])
								{
									labeltable[a] = labeltable[num];
								}
							}
						}
					}
					// Atribui a etiqueta ao pixel
					data[posX] = num;
				}
			}
		}
	}

	// Volta a etiquetar a imagem
	for(y=1; y<height-1; y++)
	{
		for(x=1; x<width-1; x++)
		{
			posX = y * bytesperline + x * channels; // X

			if(data[posX] != 0)
			{
				data[posX] = labeltable[data[posX]];
			}
		}
	}
	return 1;
}

int vc_rgb_get_green_gray(IVC *srcdst)
{
	unsigned char *data = (unsigned char *) srcdst->data;
	int width = srcdst->width;
	int height = srcdst->height;
	int bytesperline = srcdst->bytesperline;
	int channels = srcdst->channels;
	int x, y;
	long int pos;

	// Verificação de erros
	if((srcdst->width <= 0) || (srcdst->height <= 0) || (srcdst->data == NULL)) return 0;
	if(channels != 3) return 0;

	// Extrai a componente Red
	for(y=0; y<height; y++)
	{
		for(x=0; x<width; x++)
		{
			pos = y * bytesperline + x * channels;

			data[pos 	] = data[pos+1]; // Red
			data[pos + 2] = data[pos+1]; // Blue
		}
	}

	return 1;
}

int vc_histogram_equalize(IVC* srcdst)
    {
    unsigned long histogram[256]; /* image histogram */
    unsigned long sum_hist[256];  /* sum of histogram elements */
    float alpha;           /* normalized scale factor */
    unsigned long i;              /* index variable */
    unsigned long sum;            /* variable used to increment sum of hist */

    unsigned long number_of_pixels = srcdst->height * srcdst->bytesperline;

    /* clear histogram to 0 */
    for(i=0; i<256; i++)
		histogram[i]=0;

    /* calculate histogram */
    for(i=0; i<number_of_pixels; i++)
		histogram[srcdst->data[i]]++;

    /* calculate normalized sum of hist */
    	sum = 0;
    	alpha = 255.0 / number_of_pixels;
    for(i=0; i<256; i++)
	{
		sum += histogram[i];
		sum_hist[i] = (sum * alpha) + 0.5;
	}

    /* transform image using new sum_hist as a LUT */
    for(i=0; i<number_of_pixels; i++)
		srcdst->data[i] = sum_hist[srcdst->data[i]];

	return 1;
}
