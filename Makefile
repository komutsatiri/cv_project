CC=gcc
CFLAGS=-c	-Wall
LDLAGS=
LIBS=	-lm
SOURCES=main.c	functions.c	vc.c
OBJECTS=$(SOURCES:.c=.o)
EXECUTABLE=output

all:	$(SOURCES)	$(EXECUTABLE)

$(EXECUTABLE):	$(OBJECTS)
	$(CC)	$(OBJECTS)	-o	$@ $(LIBS)
.c.o:
	$(CC)	$(CFLAGS)	$<	-o $@
clean:
	rm	$(OBJECTS)	$(EXECUTABLE)
